const Course = require('../models/course.js');

const auth = require('../auth.js');

module.exports.addCourse = (req, res) => {

    const reqBody = req.body;

    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
    })

    return newCourse.save().then((newCourse, err) => {
        if (err) {
            return res.send(err);
        } else {
            return res.send(newCourse);
        }
    })

}


module.exports.getAllCourses = (req, res) => {
    return Course.find({}).then(result => {
        return res.send(result);
    })
}

module.exports.getActiveCourses = (req, res) => {
    return Course.find({isActive: true}).then(result => {
        return res.send(result);
    })
}


module.exports.getCourseById = (req, res) => {
    return Course.findById(req.params.id).then(result => {
        return res.send(result);
    })
}


module.exports.updateCourse = (req, res) => {
    const newData = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    
    const courseId = req.params.id


    if (newData.isAdmin == true) {
        return Course.findByIdAndUpdate(courseId,
        {
            name: newData.course.name,
            description: newData.course.description,
            price: newData.course.price
        },(err, updatedCourse) => {
            if (err) {
                res.send(false);
            }
            
            return res.send(updatedCourse);

        });
    } else {
        let message = Promise.resolve('User must be ADMIN to access this.');
        return res.send('User must be ADMIN to access this.');
    }
}