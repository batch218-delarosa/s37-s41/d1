const express = require("express");
const userRouter = express.Router();


const userController = require('../controllers/userController.js');


userRouter.post('/checkEmail', userController.checkEmailExist);

userRouter.post('/register', userController.registerUser);

userRouter.post('/login', userController.loginUser);

userRouter.post('/details', userController.getUserDetails);


module.exports = userRouter;