const express = require("express");
const courseRouter = express.Router();

const courseController = require('../controllers/courseController.js');

const auth = require('../auth.js');

courseRouter.post('/create', courseController.addCourse)

courseRouter.get('/all', courseController.getAllCourses)

courseRouter.get('/active', courseController.getActiveCourses)

courseRouter.get('/:id', courseController.getCourseById)

courseRouter.patch('/:id/update', auth.verify, courseController.updateCourse);

module.exports = courseRouter;